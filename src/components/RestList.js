import React from 'react';
//import RestLi from './RestLi'
import EditLi from './EditLi'

function RestList(props) {

    const {listTitle, author, restaurants, expand} = props;
    var expand_div_class = "no-display"
    var rest_div_class = "restauarants"
    var restcont_class = "rest-container soft-shadow"
    var author_h4_class,img,tags
    console.log(expand)


    //var myrests
    //THis is where you would load the list from the database baesd on the title
    /*
    if (listTitle === "Second List") { 
        myrests = ["Starbucks", "Cafe", "Lost dog Cafe", "Pho 66", "Kung Fu Tea", "Blaze Pizza", "Taco Bar", "Verizon Wireless"];
    } else{
        myrests = ["Hello", "goodbye", "test", "best", "Chipotle"];
    }
    for (let rest of restaurants){
        myrests
    }
    */


        /*
    function AddEditLi(){
        console.log("lkjdslakjfds");
        myrests.push("new");
    }
    */
    function ToggleContents(event){
        var targ= event.target
        if (targ.nextSibling.nextSibling.className.includes("no-display")){
            targ.nextSibling.nextSibling.className="restaurants";
            targ.nextSibling.nextSibling.nextSibling.className="no-display";
        } else{
            targ.nextSibling.nextSibling.className="restaurants, no-display";
            targ.nextSibling.nextSibling.nextSibling.className="show-more";
        }


    }
    function mostCommonTags(){
        return("hello,goodbye,hard-coded,testing".split(','))
        }
    function RenderTags(){
        var tagarray=[]
        for (let tag of mostCommonTags()){
            tagarray.push(<span className="tag softer-shadow">#{tag}</span>);
        }
        return(<div className="tags flex-column">{tagarray}</div>);
        
    }

    if ( expand === false ){
        restcont_class="rest-container collapsed soft-shadow"
        rest_div_class="restaurants, no-display";
        expand_div_class="show-more"
        author_h4_class="no-display"
        img=<img src={restaurants[0].picture} alt="pinned restaurant" />
        tags=RenderTags()
    } else{
        img=null
        tags=null
    }
    
    return (
        <div class={restcont_class}>
            <div class="flex-row">
              {img}
              <div>
                <h2 className=".list-title" onClick={ToggleContents}>{listTitle}</h2>
                <h4 className={author_h4_class}>owned by <a href="profile.html?u={author}">{author}</a></h4>
                <ul className={rest_div_class}>
                    { restaurants.map(r => (
                    <EditLi rest={r.name} tags={r.tag} pic={r.picture} url={r.url} />
                    )) 
                    }
                 </ul>
                 <div class={expand_div_class}>...</div>
              </div>
              {tags}
            </div>
         </div>
    );
}
                // <li><button onClick={AddEditLi()}>+</button></li>

export default RestList;
