import React from 'react';

class Pin extends React.Component {
    constructor(props){
        super(props);
        this.state={
            hello:"hello",
            listid:props.id,
            list:props.list,
            filled:props.list==null? "pin-empty" : "pin"
        }
    }
    render(){
        var pin = <button class="fas fa-thumb-tack"/>
        var contents=[];
        if (this.state.list == null) {
            contents.push(pin)

        }else{
            var h3= <h3>{this.state.list.title}</h3>
            var image=<img src={this.state.list.restaurants[0].picture} alt="pinned restaurant" />
            contents.push(pin)
            contents.push(image)
            contents.push(h3)
        }
        return(
            <div className={this.state.filled+" flex-column"}>
                {contents}
            </div>
        )
    }
}
export default Pin ;
