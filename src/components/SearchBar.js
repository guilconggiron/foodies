import React from 'react';
import { jsonResponse } from '../json/searchApi.js';

class SearchBar extends React.Component {
    constructor(props){
        super(props);
        this.state={
            contents: "",
            dropdown: false
        }
        this.requestResults=this.requestResults.bind(this);
        this.lazySearch=this.lazySearch.bind(this);
        this.searchSubmit=this.searchSubmit.bind(this);
        this.resultDialog=this.resultDialog.bind(this);

    }

    requestResults(){
        /*const apiResult = require('../json/api-search-fake.json');
        console.log(JSON.parse(apiResult));
        // this.resultDialog();
        // */
        const json=JSON.parse(JSON.stringify(jsonResponse));
        var titleUrl=[];
        for (let result of json.responseData.results){
            titleUrl.push([result.title,result.url]);
            
        }
        return(titleUrl);
    }

    lazySearch(event){
        //implement a lazy search if idle for 2 seconds, eg
        var lazy =event.target.value;
        this.setState({ content:lazy, dropdown: true});
        //console.log(lazy);
        //this.resultDialog(lazy);
        //thisRequestResults()
    }

    searchSubmit(event){
        event.preventDefault();
        var query=document.getElementById('searchbar-input').value;
        console.log(query);
        this.requestResults();
        
    }
    resultDialog(){
        console.log("hello");
        //var query = document.getElementById('searchbar-input').value;
        //var liDefault= <li>{this.state.content}{returnSpan}</li>;
        var contenturl="https://foodies.alksjdflksjdf/?q="+this.state.content
        var liDefault= [this.state.content,contenturl]
        //Do i  need to limit length?
        //var returnSpan= <span >↵</span>;

        var searchResults=this.requestResults();
        searchResults.push(liDefault);
        var formattedResults=[]
        formattedResults.push( <li><a href={searchResults[0][1]}>{searchResults[0][0]}<span >↵</span></a></li>);
        for ( var i =1; i<searchResults.length; i++){
            formattedResults.push(<li><a href={searchResults[i][1]} >{searchResults[i][0]}</a></li>)
        }

        return(
            <ul>{formattedResults}</ul>
        );

        /*
        if(document.getElementById("search-drop")!= null ){
            console.log("thats whatim oding");
            document.getElementById("search-drop").value=query

        }else{
            console.log("not what im doing")
            document.getElementById("search-li").append(searchdrop);
            
        }
        var list = document.getElementsByClassName("events");
        for (let item of list) {
                console.log(item.id);
            }
        */
    }


    componentDidMount(){
        /*
        document.getElementById("searchbar-input.searchinput.addEventListener("keyup", function(event) {
          if (event.keyCode === 13) {
            event.preventDefault();
            document.getElementById("searchbar-input").click();
        } });
        */

       }


 
    render(){
        var searchinput= <input id="searchbar-input" type="text" onChange={this.lazySearch}></input> ;
        var searchbu = <button onClick={this.searchSubmit} className="fas fa-search"></button>
        var form = <form autoComplete="off" id="searchbar" onSubmit={this.searchSubmit}> {searchinput} {searchbu} </form> 
            //var searchdrop = <div id="search-drop" class="soft-shadow">{this.resultDialog()}</div> ;
            //var searchdrop = <div id="search-drop" class="soft-shadow"></div> ;
       
        var contents
        if (this.state.dropdown ===true){
            contents=<div id="search-drop" class="soft-shadow">{this.resultDialog()}</div> ;
        }
            /*
        if (this.state.dropdown ===true){
            return( 
                <div>{form} {searchdrop}</div>
            )
        }
        */

        return(<div>{form}{contents}</div>);
    } 

}
export default SearchBar;
