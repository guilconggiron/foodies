import React from 'react';
import {jsonify, restListJson} from '../json/jsonify';
import {myProfile} from '../json/myProfile';

class UserProfile  extends React.Component {
    constructor(props){
        super(props);
        var json=jsonify(myProfile).reply
        this.state={
            authentication:0,
            user_id: props.userId,
            user_real_name: json.user.realname,
         //   user_picture: json.user.picture,
            user_picture: this.userDetails(props, json),
            carousel_pics: json.user.carousel,
            restLists: restListJson(myProfile.reply,false), 
            json:json
        }

        //this.loggedIn();
        this.userDetails=this.userDetails.bind(this)
        this.carousel=this.carousel.bind(this);

    }

    userDetails(props, json){
        //fetch
        
        if (props.userId.includes("straw")){
            //this.setState({ user_picture : "/images/profile_straw.png" });
            return "/images/profile_straw.png"; }
        else{ return json.user.picture; }
    }
    carousel(){
        var carousel=[]
        for (let pic of this.state.carousel_pics){
            console.log(pic);
            carousel.push(<div class="img-wrapper"><img class="hover-resize" src={pic} alt="{caption}" /> </div>);
        }

        return carousel
    }
    totalRestaurants = () =>{
	var total=0
         for (let list of this.state.json.lists){
             console.log(list)
	     total+=list.restaurants.length
         }
         return total;
     }
    

    getActions = () =>{
        var addfriend = <button class="fas fa-user-plus"/>
        var edit = <button class="fas fa-edit"/>
        var msg = <button class="fas fa-comment"/>
        var share = <button class="fas fa-share-square"/>
            //var invite = <button class="fas fa-envelope-open-text"/>
            //var invite = <input type="image" src="/res/invitation.svg"/>
            var invite = <button id="invite"><img class="softer-shadow" alt="invitation icon" src="/foodies/res/invitation_yellow.svg"/></button>
        if (this.state.authentication < 2){
            return ([addfriend, msg, share, edit, invite])
        }
    }

 
    render(){
        //const authenitcation= this.state.authentication
        var profile_pic=<div class="img-wrapper"><img class="user-picture soft-shadow" src={this.state.user_picture} alt="default profile fork" /></div>
        var h1=<h1>{this.state.user_real_name} ({this.state.user_id})</h1>
            //var carousel = <div id="carousel"><div class="img-wrapper"><img src="/images/sample_dish3.png" /></div> <div class="img-wrapper"><img src="/images/sample_kbbq2.png"/><div class="img-wrapper"><img src="/images/sample_dish.png" /></div> <div class="img-wrapper"><img src="/images/sample_kbbq3.png"/> <div class="img-wrapper"><img src="/images/sample_dish2.png" /></div>  <div class="img-wrapper"><img src="/images/sample_kbbq2.png"/> <div class="img-wrapper"><img src="/images/sample_dish3.png" /></div> <div class="img-wrapper"><img src="/images/sample_kbbq2.png"/><div class="img-wrapper"><img src="/images/sample_dish.png" /></div> <div class="img-wrapper"><img src="/images/sample_kbbq3.png"/> <div class="img-wrapper"><img src="/images/sample_dish2.png" /></div>  <div class="img-wrapper"><img src="/images/sample_kbbq2.png"/></div>

        return(
            <div>
                <div id="user-wrapper" class="flex-row">
                    <div id="stats-wrapper" class="flex-column">
                        <div>{profile_pic}</div>
                    </div>
                    <div class="flex-column">
                        {h1}
                        <div class="stats">This user has {this.state.restLists.length} lists, {this.state.carousel_pics.length} featured pictures,  and {this.totalRestaurants()} restaurants.</div>
                        <div id="bio">This is my biooooooooooooooooooooooooooooooooo</div>
                        <div class="profile-actions flex-row">{this.getActions()}</div>
                    </div>
                </div>
                <div id="carousel" class="flex-row">{this.carousel()}</div>
            <div class="list-container" expanded={true} > {this.state.restLists}</div>
            
        </div>

        )
    } 

}
export default UserProfile;
