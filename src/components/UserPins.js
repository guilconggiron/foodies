import React from 'react';
import Pin from './Pin';
import {jsonify} from '../json/jsonify';

class UserPins  extends React.Component {
    constructor(props){
        const TOTAL=4;
        var pinned=[2];
        super(props);
        this.state={
            lists:jsonify(props.lists).lists,
            pinned:[2],
            empties:TOTAL-pinned.length,
        }
    }
    findListById = (id) =>{
        var lists=this.state.lists
        return lists.filter(
            function(lists){ return lists.id === id } 
        )[0];

    }
    render(){
        console.log("hello")
        console.log(this.findListById("2"))
        console.log("that was the find function");
        var list_array=[]
        var empty_array=[]
        for (let l of this.state.pinned){
            var list= this.findListById(l.toString())
            list_array.push(<Pin list={list} />)
        }
        for (var i=0; i<this.state.empties; i++){
            empty_array.push(<Pin id={i}/>)

        }
        return(
            <div id="pins" className="flex-row">
                {list_array}
                {empty_array}
            </div>
        )
    }
}
export default UserPins
