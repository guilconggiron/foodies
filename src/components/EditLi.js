import React from 'react';
/*
 * edit:0 active:0 <li> <span>  restaurant </span>                          </li>
 * edit:0 active:1 <li> <span>  restaurant </span>  <button> Edit </button> </li>
 ******************************************************** EDIT IS CLICKED********
 * edit:1 active:1 <li> <input> restaurant </input> <button> Save </button> </li>
 ******************************************************** SAVE IS CLICKED********
 * edit:0 active:1 <li> <span>  restaurant2 </span> <button> Edit </button> </li>
 *
 *
 */

class EditLi extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
          edit: false,
          active: false,
          restaurant : props.rest,
          url : props.url,
          picture : props.pic,
          tags: props.tags.split(",")
        };

        this.ClickSpan=this.ClickSpan.bind(this)
        this.ClickBu=this.ClickBu.bind(this)
        this.ButtonText=this.ButtonText.bind(this)
        this.ButtonClass=this.ButtonClass.bind(this)
        this.RenderTags=this.RenderTags.bind(this)

    }
     
     
    ClickSpan(){
        // if it is not in edit mode, clicking the span will make the active:true 
        if (this.state.edit === false) {
            this.setState({ active: !this.state.active })
        }
    }

    ClickBu(){ 
        if ( this.state.edit === false) {    //EDIT is clicked, so EDIT MODE TURNED ON (edit = true)
            this.setState({ edit: true });
            //this.forceUpdate();
        } else {                             //SAVE is clicked, so turn off EDIT MODE and reassign variable.
            this.setState({restaurant:document.getElementById('edit-input').value});
            //This is done improperly right now. You can edit any number of EditLis thus there can be any number of elements by ID edit-input
            this.setState({ edit: false });
                                            // UNSURE IF I WANT TO KEEP THIS DECISION, but I want to turn off active after saving
            //this.setState({ active:false});
        }

    }
    
    ButtonText(){
        //not used anymore because I switched to icons
        if ( this.state.edit === false) { 
            return "Edit"; 
        } else {
            return "Save";
        }
    }
    ButtonClass(){
        if ( this.state.edit === false) { 
            return "fas fa-edit"; 
        } else {
            return "fas fa-save";
        }
    }
    RenderTags(){
        var tagarray=[]
        for (let tag of this.state.tags){
            tagarray.push(<span className="tag softer-shadow">#{tag}</span>);
        }
        return(<div className="tags flex-row">{tagarray}</div>);
        
    }

    myUrl(){
        if(this.state.url.length>0){
            return this.state.url
        } else{
            return "Set an External URL!"
        }
    }
    render() {
        //var span_hover = <span onClick={this.ClickSpan} onMouseEnter={()=>this.setState({ active:true }) }>{this.state.restaurant}</span>;
        var span = <span onClick={this.ClickSpan} >{this.state.restaurant}</span>;
        var input = <input type="text" id="edit-input" defaultValue={this.state.restaurant} / >;
        var button = <button onClick={() => this.ClickBu()} class="soft-shadow">{this.ButtonText()}</button>
        var pic = <img class= "restaurant-pic" src={this.state.picture} alt={this.state.restaurant} />
        var url = <div> External site:<a href={this.myUrl()}>{this.myUrl()} <span class="fas fa-external-link-square-alt"></span></a></div>
        var details= <div class="details"> {url}<div class="flex-row">{pic} {this.RenderTags()}</div></div>
        //var button = <button onClick={() => this.ClickBu()} class="soft-shadow">{this.ButtonText()}</button>
            var edit_span = <span> <button class={this.ButtonClass()} onClick={() => this.ClickBu()} label="edit"/><button class="fas fa-clone" label="clone" /><button class="fas fa-trash" label="delete" /></span>
        //referring to visual at top of doc
        if ( this.state.edit === false ){            //edit:0
            if (this.state.active === false){              //active:0
                //return <li onMouseEnter={this.setState({active: true })} onMouseLeave={this.setState({ active:false } )}>{span}</li>; 
                //implement hover another time
                //return <li>{span_hover}</li>; 
                //I cant use hover because when you toggle to close your mouse is still over it and it expands again. How to prevent this?
                return <li>{span}</li>;
            } else {                                       //active:1
                //return <li>{span} {button}</li>;
                //i onMouseLeave={this.ClickSpan}
                return <li><div>{span} {edit_span} </div> {details}</li>;
            }  
        } else {                                     //edit:1
            if (this.state.active === false){                //active:0
                return <li>{span} {edit_span}</li>;
            } else {                                       //active:1
                return <li><div>{input} {edit_span} </div> {details}</li>;
        }   }
    }
}
export default EditLi;
