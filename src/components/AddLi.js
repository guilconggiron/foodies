import React from 'react';

/* TO BE IMPLEMENTED */

class AddLi extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
          edit: false,
          active: false,
          restaurant : props.rest
        };

        this.ClickSpan=this.ClickSpan.bind(this)
        this.ClickBu=this.ClickBu.bind(this)
        this.ButtonText=this.ButtonText.bind(this)

    }
     
     
    ClickSpan = () => {
        // if it is not in edit mode, clicking the span will make the active:true 
        if (this.state.edit === false) {
            this.setState({ active: !this.state.active })
        }
    }

    ClickBu = () =>{ 
        if ( this.state.edit === false) {    //EDIT is clicked, so EDIT MODE TURNED ON (edit = true)
            this.setState({ edit: true });
            this.forceUpdate();
        } else {                             //SAVE is clicked, so turn off EDIT MODE and reassign variable.
            this.setState({restaurant:document.getElementById('edit-input').value});
            //This is done improperly right now. You can edit any number of EditLis thus there can be any number of elements by ID edit-input
            this.setState({ edit: false });
                                            // UNSURE IF I WANT TO KEEP THIS DECISION, but I want to turn off active after saving
            this.setState({ active:false});
        }

    }
    
    ButtonText = () => {
        if ( this.state.edit === false) { 
            return "Edit"; 
        } else {
            return "Save";
        }
    }

    render() {
        var span = <span onClick={this.ClickSpan}>{this.state.restaurant}</span>;
        var input = <input type="text" id="edit-input" defaultValue={this.state.restaurant} / >;
        var button = <button onClick={() => this.ClickBu()}>{this.ButtonText()}</button>
        //referring to visual at top of doc
        if ( this.state.edit === false ){            //edit:0
            if (this.state.active === false){              //active:0
                //return <li onMouseEnter={this.setState({active: true })} onMouseLeave={this.setState({ active:false } )}>{span}</li>; 
                //implement hover another time
                return <li>{span}</li>;
            } else {                                       //active:1
                console.log("alkjdslfkajsldkfjlaskdjf")
                return <li>{span} {button}</li>;
            }  
        } else {                                     //edit:1
            if (this.state.edit === false){                //active:0
                return <li>{span} {button}</li>;
            } else {                                       //active:1
                return <li>{input} {button}</li>;
        }   }
    }
}
export default EditLi;
