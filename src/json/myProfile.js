const myProfile= {
    "reply":{
        "user": {
            "realname": "Foodies Foodster McFoodwagon",
            "username": "foodwagamillion365",
            "picture": "/foodies/images/profile_fork.png",
            "carousel": [
                "/foodies/images/sample_dish3.png",
                "/foodies/images/sample_kbbq.png",
                "/foodies/images/sample_dish2.png",
                "/foodies/images/sample_kbbq2.png",
                "/foodies/images/sample_dish.png",
                "/foodies/images/sample_kbbq3.png",
                "/foodies/images/sample_dish3.png",
                "/foodies/images/sample_kbbq.png",
                "/foodies/images/sample_dish2.png",
                "/foodies/images/sample_kbbq2.png",
                "/foodies/images/sample_dish.png",
                "/foodies/images/sample_kbbq3.png"
            ]
        },
        "lists": [
            {
                "id": "1",
                "url": "https://foodies/alskjdlfkjsjdfkjdf/?list=asldjf",
                "title": "Comfy Hip Spots",
                "restaurants":[
                    {
                        "name": "comfy hip spot 1",
                        "tag": "comfy,hip,lunch,cheap",
                        "url": "https://yelp/aslkfdjalksdjf",
                        "picture": "/foodies/images/sample_dish.png",
                    },
                    {
                        "name": "comfy hip spot 2",
                        "tag": "comfy,hip,lunch,cheap,testing,a lot of tags, a lot",
                        "url": "https://yelp/aslkfdjalksdjf",
                        "picture": "/foodies/images/sample_dish2.png",
                    },
                    {
                        "name": "comfy hip DINNER",
                        "tag": "comfy,hip,dinner,cheap",
                        "url": "https://yelp/aslkfdjalksdjf",
                        "picture": "/foodies/images/sample_dish3.png",
                    },
                ]
            },
            {
                "id": "2",
                "url": "https://foodies/alskjdlfkjsjdfkjdf/?list=asldjf",
                "title": "kbbq favs",
                "restaurants":[
                    {
                        "name": "kbbq1",
                        "tag": "dinner,expensive",
                        "url": "https://yelp/aslkfdjalksdjf",
                        "picture": "/foodies/images/sample_kbbq.png",
                    },
                    {
                        "name": "kbbq2",
                        "tag": "comfy,large groups,expensive",
                        "url": "https://yelp/aslkfdjalksdjf",
                        "picture": "/foodies/images/sample_kbbq2.png",
                    },
                    {
                        "name": "kbbq23",
                        "tag": "large groups,dinner,dessert",
                        "url": "https://yelp/aslkfdjalksdjf",
                        "picture": "/foodies/images/sample_kbbq3.png",
                    }
                ]
            }
        ]
    }
};

export {myProfile};
