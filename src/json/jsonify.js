import React from 'react';
import RestList from '../components/RestList'

export function jsonify(props) {
    return JSON.parse(JSON.stringify(props));
}
export function restListJson(props, expand=true){
    var json=jsonify(props)
    var RestLists=[]
    for (let list of json.lists){
        RestLists.push(<RestList listTitle={list.title} author="me" expand={expand} restaurants={list.restaurants} />)
    }
    return RestLists
}

export default restListJson;
