import React from 'react';
import './App.css';
//import RestList from './components/RestList'
import NavBar from './components/NavBar'
import UserProfile from './components/UserProfile'
import Home from './components/Home'
import { myLists } from './json/myListsApi';
import restListJson from './json/jsonify';
import {
    BrowserRouter as Router,
    Switch, 
    Route,
    useRouteMatch,
    useParams,
    //Link
} from "react-router-dom";

function App() {
    var RestLists=restListJson(myLists);

  return (
<Router >
  <div>
    <NavBar />
    <div class="App">

    <Switch>
	<Route path="/settings">
	    <h1>Settings!</h1>
	</Route>
	<Route path="/list">
	    <h1>List page</h1>
	    <div id="list-container">
		<div id="list-actions">
		    <h2 class="fas fa-list-util">List Actions</h2>
		    <ul>
			<li>Create a list</li>
			<li>Manage lists</li>
			<li>Delete lists</li>
			<li>Share lists</li>
			<li>List Defaults</li>
			<li>List Settings</li>
		    </ul>
		</div>

		<div id="list-wrapper">
		    {RestLists}
		</div>
	    </div>
	</Route>
        <Route path="/profile">
            <Users />
	</Route>
	<Route path="/">
              <Home />

	</Route>
    </Switch>

    </div>
</div>
</Router>
  );
}
function Users(){
    var match=useRouteMatch();

    return(
        <Router>
            <Route path={`${match.path}/:userId`}>
                <User />
            </Route>
            <Route path="{match.path}">
		<UserProfile/>
            </Route>
        </Router>
    )
}
function User(){
    let { userId } = useParams();
    return <UserProfile userId={userId} />;
}

export default App;
